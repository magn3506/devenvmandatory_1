# devEnvMandatory_1

This is a project created for KEA web-development by Jakob Falkenberg & Magnus V. Jensen
The projects aims to showcase automated DevOps for an todolist application created with, React, Node.js, Express, using a JSON file as DB

## Documentation Site

You find the documentation here:
[Click here for documentation](https://jako2889.gitlab.io/devenvmandatory_1_docs/)
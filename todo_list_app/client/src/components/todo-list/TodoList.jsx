
import React, { Component } from 'react';

export default class TodoList extends Component {


    render() {

        const { todoList } = this.props;

        const list = todoList.map(e => {

            return (
                <li key={e.id} >{e.task}</li>
            );
        })

        return (

            <ul>
                {list}
            </ul>

        )
    }
}

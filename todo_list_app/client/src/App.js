import React, { Component } from 'react'
import TODO_LIST from './components/todo-list/TodoList';
import call_api from './functions/callApi';


export class App extends Component {

  constructor() {

    super();

    this.state = {
      isLoading: true
      ,
      dataList: [
        {
          task: "hehe",
          id: 1
        }
      ]
    }
  }

  componentDidMount() {

    call_api("http://localhost:9000/")
      .then(response => {

        setTimeout(() => {
          this.setState({
            dataList: response.data.todos,
            isLoading: false
          })
        }, 1000)
      })
  }

  render() {


    const { isLoading } = this.state;

    return (
      <div>
        {isLoading ? "LOADING" : <TODO_LIST todoList={this.state.dataList} />}
      </div>
    )
  }
}

export default App

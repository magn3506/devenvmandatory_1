const express = require('express');
// const path = require('path');

const jsonData = require('./data.json');

const app = express();
const PORT = process.env.PORT || 9000;

// Serve static files from the React app
/*
app.use(express.static(path.join(path.resolve(__dirname, '..'), 'client/build')));
*/

// MIDDLEWARE

// ALLOW CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'PATCH, POST, GET, DELETE');
  next();
});

app.use(express.urlencoded({ extended: true })); // ??
app.use(express.json()); // ??

// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.

app.get('/', (req, res) => {
  res.send(jsonData);
});

/*
app.get('*', (req, res) => {
    res.sendFile(path.join(path.resolve(__dirname, '..'), '/client/build/index.html'));
});
*/

// LISTEN
app.listen(PORT, (err) => {
  if (err) {
    return '';
  }
  return '';
});

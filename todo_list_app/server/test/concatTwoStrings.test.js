const concatTwoStrings = require('./concatTwoStrings');

test('Concat two strings Abe + Cat = AbeCat', () => {
  expect(concatTwoStrings('Abe', 'Cat')).toBe('AbeCat');
});
